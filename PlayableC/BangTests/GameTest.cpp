#include "stdafx.h" //commented to avoid linker erorrs.
#include <string>
#include <iostream>
#include <cassert>
#include "..\PlayableC\Game.h"
#include "..\PlayableC\PermanentCard.h"
#include "..\PlayableC\GunCard.h"
#include "..\PlayableC\BangCard.h"
#include "..\PlayableC\DiscardCard.h"
#include "..\PlayableC\DrawCard.h"

using namespace std;

void testGameDistance() {
	Game game;
	Player * Player1 = new Player();
	Player * Player2 = new Player();
	Player * Player3 = new Player();
	Player * Player4 = new Player();
	game.addPlayer(Player1);
	game.addPlayer(Player2);
	game.addPlayer(Player3);
	game.addPlayer(Player4);
	Player1->setLife(2);
	Player2->setLife(2);
	Player3->setLife(2);
	Player4->setLife(2);

	assert(game.getPlayers().size() == 4);
	assert(game.getDistance(Player1, 1) == 1);
	assert(game.getDistance(Player1, 2) == 2);
	assert(game.getDistance(Player1, 3) == 1);

	Player2->setLife(0);

	assert(game.getPlayers().size() == 4);
	assert(game.getDistance(Player1, 2) == 1);
	assert(game.getDistance(Player1, 3) == 1);

	Player2->setLife(2);

	assert(game.getPlayers().size() == 4);
	assert(game.getDistance(Player1, 1) == 1);
	assert(game.getDistance(Player1, 2) == 2);
	assert(game.getDistance(Player1, 3) == 1);

	shared_ptr<PermanentCard> appalossa = shared_ptr<PermanentCard>(new PermanentCard("Appalossa", "", 1));
	appalossa->setDistanceTweak(-1);
	Player1->addCard(appalossa);
	Player1->layCard(0);

	assert(game.getDistance(Player1, 1) == 1);
	assert(game.getDistance(Player1, 2) == 1);
	assert(game.getDistance(Player1, 3) == 1);
	assert(game.getDistance(Player2, 0) == 1);
	assert(game.getDistance(Player3, 0) == 2);
	assert(game.getDistance(Player4, 0) == 1);

	shared_ptr<PermanentCard> mustang = shared_ptr<PermanentCard>(new PermanentCard("Mustang", "", 1));
	mustang->setDistanceTweak(1);
	Player2->addCard(mustang);
	Player2->layCard(0);

	assert(game.getDistance(Player3, 1) == 2);
	assert(game.getDistance(Player1, 1) == 1);
}

void testGameUnlimitedBang() {
	Game game;
	Player * a = new Player();
	Player * b = new Player();
	game.addPlayer(a);
	game.addPlayer(b);
	a->setLife(3);
	b->setLife(3);

	shared_ptr<BangCard> bang = shared_ptr<BangCard>(new BangCard("Bang!", "", 2));
	bang->setTargetDistance(true);
	shared_ptr<GunCard> volcanic = shared_ptr<GunCard>(new GunCard("Volcanic", "", 1, 1));
	volcanic->setUnlimitedBang(true);
	a->addCard(bang);
	a->addCard(bang);
	a->addCard(volcanic);
	a->layCard(2);
}

void testGameDiscardCard() {
	Game game;
	Player * a = new Player();
	Player * b = new Player();
	game.addPlayer(a);
	game.addPlayer(b);
	a->setLife(1);
	b->setLife(1);

	shared_ptr<GunCard> volcanic = shared_ptr<GunCard>(new GunCard("Volcanic", "", 1, 1));
	shared_ptr<DiscardCard> discard = shared_ptr<DiscardCard>(new DiscardCard("Cat Balou", "", 1));
	discard->setTargetAny(true);

	a->addCard(discard);
	b->addCard(volcanic);
	b->layCard(0);
	assert(b->getPermanentCards().size() == 1);
	assert(a->getCards().size() == 1);
	assert(b->getPermanentCards().size() == 0);
	assert(a->getCards().size() == 0);
}

void testGameDrawCard() {
	Game game;
	Player * a = new Player();
	Player * b = new Player();
	game.addPlayer(a);
	game.addPlayer(b);
	a->setLife(1);
	b->setLife(1);

	shared_ptr<GunCard> volcanic = shared_ptr<GunCard>(new GunCard("Volcanic", "", 1, 1));
	shared_ptr<DrawCard> draw = shared_ptr<DrawCard>(new DrawCard("Panic", "", 1, 1));
	draw->setTargetDistance(true);

	a->addCard(draw);
	b->addCard(volcanic);
	b->layCard(0);
	assert(b->getPermanentCards().size() == 1);
	assert(a->getCards().size() == 1);
	assert(a->getCards().size() == 1);
	assert(b->getPermanentCards().size() == 0);
	assert(a->getCards().size() == 1);
}

void testGameOver() {
	Game game;
	Player * a = new Player();
	Player * b = new Player();
	Player * c = new Player();
	Player * d = new Player();
	game.addPlayer(a);
	game.addPlayer(b);
	game.addPlayer(c);
	game.addPlayer(d);

	RoleCard * sheriff = new RoleCard(RoleCard::SHERIFF, "", 1);
	RoleCard * renegate = new RoleCard(RoleCard::RENEGATE, "", 1);
	RoleCard * outlaw = new RoleCard(RoleCard::OUTLAW, "", 2);
	CharacterCard * character = new CharacterCard("Paul Regret", "", 1);
	character->setLife(3);

	a->setRole(sheriff);
	b->setRole(renegate);
	c->setRole(outlaw);
	d->setRole(outlaw);
	a->setCharacter(character);
	b->setCharacter(character);
	c->setCharacter(character);
	d->setCharacter(character);
	a->setLife(a->getMaxLife());
	b->setLife(b->getMaxLife());
	c->setLife(c->getMaxLife());
	d->setLife(d->getMaxLife());

	assert(game.getWinners().size() == 0);
	a->setLife(0);
	assert(game.getWinners().size() == 1);
	assert(game.getWinners()[0] == RoleCard::OUTLAW);
	c->setLife(0);
	d->setLife(0);
	assert(game.getWinners().size() == 1);
	assert(game.getWinners()[0] == RoleCard::RENEGATE);

	delete sheriff;
	delete renegate;
	delete outlaw;
	delete character;
}

void testGame() {
	testGameDistance();
	testGameUnlimitedBang();
	testGameDiscardCard();
	testGameDrawCard();
	testGameOver();
}
