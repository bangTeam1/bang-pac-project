#include <cassert>
#include "..\PlayableC\CardParser.h"
#include "..\PlayableC\Card.h"
#include "..\PlayableC\RoleCard.h"
#include "..\PlayableC\CharacterCard.h"
#include "..\PlayableC\GunCard.h"
#include "..\PlayableC\Card.h"
#include <vector>
//#include "stdafx.h"

void testCardsParser() {
	CardsParser parser;
	vector<Card *> cards = parser.parseFile("..\PlayableC\cards.csv");

	assert(cards.size() == 42 - 6);
	RoleCard * role = (RoleCard *)cards[0];
	assert(role->getOriginalName() == "Sceriffo");
	assert(role->getName() == "�erif");

	CharacterCard * character = (CharacterCard *)cards[4];
	assert(character->getOriginalName() == "Bart Cassidy");
	assert(character->getName() == "Bart Cassidy");
	assert(character->getLife() == 4);

	GunCard * volcanic = (GunCard *)cards[31];
	assert(volcanic->getOriginalName() == "Volcanic");
	assert(volcanic->getDistance() == 1);
	assert(volcanic->hasUnlimitedBang());

	for (Card *card : cards) {
		delete card;
	}
}
