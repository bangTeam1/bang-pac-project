#include<iostream>

#include <winsock2.h>   // contains most of the Winsock functions, structures, and definitions
#include <ws2tcpip.h>   // contains newer functions and structures used to retrieve IP addresses

#pragma comment(lib, "Ws2_32.lib")  //  indicates to the linker that the Ws2_32.lib

int main(int argc, char* argv[])
{
	const int isTrue = 1;
	const int isFalse = 0;
	// Validate the parameters
	if (argc != 2) {
		std::cout << "usage" << argv[0] << " server-name";// one server
		return isTrue;
	}

	// *** initialize Winsock ***
	// initialize Winsock before making other Winsock functions calls (only once per application/dll)
	WSADATA wsaData;
	int iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);     // initiate use of WS2_64.dll, with version 2.2
	if (iResult != 0)
	{
		std::cerr << "Can'tinitialize winsock! Faild" << iResult;
		return isTrue;
	}

	// *** Create a socket ***
	addrinfo *result = nullptr, *ptr = nullptr, hints;

	ZeroMemory(&hints, sizeof(hints));  // memset to 0
	hints.ai_family = AF_UNSPEC;        // unspecified: either an IPv6 or IPv4
	hints.ai_socktype = SOCK_STREAM;    // stream socket
	hints.ai_protocol = IPPROTO_TCP;    // TCP protocol

#define DEFAULT_PORT "27015"

	// Resolve the server address and port
	iResult = getaddrinfo(argv[1], DEFAULT_PORT, &hints, &result);
	if (iResult != 0)
	{
		std::cerr << "getaddrinfo failed: " << iResult << "\n";
		WSACleanup();   // note: use WSACleanup when done working with sockets
		return 1;
	}

	SOCKET ConnectSocket = INVALID_SOCKET;
	// Attempt to connect to the first address returned by the call to getaddrinfo
	ptr = result;

	// Create a SOCKET for connecting to server
	ConnectSocket = socket(ptr->ai_family, ptr->ai_socktype, ptr->ai_protocol);

	if (ConnectSocket == INVALID_SOCKET)
	{
		std::cout << "Error at socket()" << WSAGetLastError();
		freeaddrinfo(result);
		WSACleanup();
		return isTrue;
	}

	// *** Connect to server ***
	iResult = connect(ConnectSocket, ptr->ai_addr, (int)ptr->ai_addrlen);
	if (iResult == SOCKET_ERROR)
	{
		closesocket(ConnectSocket);
		ConnectSocket = INVALID_SOCKET;
	}

	// Should really try the next address returned by getaddrinfo
	// if the connect call failed
	// But for this simple example we just free the resources
	// returned by getaddrinfo and print an error message

	freeaddrinfo(result);

	if (ConnectSocket == INVALID_SOCKET)
	{
		std::cout << "Unable to connect to server!\n";
		WSACleanup();
		return isTrue;
	}

	// *** Send data to server ***
#define DEFAULT_BUFLEN 512

	int recvBufferLenght = DEFAULT_BUFLEN;

	const char *sendBuffer = "this is a test";
	char recvbuffer[DEFAULT_BUFLEN];

	// Send an initial buffer
	iResult = send(ConnectSocket, sendBuffer, (int)strlen(sendBuffer), 0);
	if (iResult == SOCKET_ERROR)
	{
		std::cout << "send failed:" << WSAGetLastError() << "\n";
		closesocket(ConnectSocket);
		WSACleanup();
		return isTrue;
	}

	std::cout << "Bytes Sent:" << iResult;

	// shutdown the connection for sending since no more data will be sent
	// the client can still use the ConnectSocket for receiving data
	iResult = shutdown(ConnectSocket, SD_SEND);
	if (iResult == SOCKET_ERROR)
	{
		std::cout << "shutdown failed:" << WSAGetLastError() << "\n";
		closesocket(ConnectSocket);
		WSACleanup();
		return isTrue;
	}

	// Receive data until the server closes the connection
	do
	{
		iResult = recv(ConnectSocket, recvbuffer, recvBufferLenght, 0);
		if (iResult > 0)
		{
			std::cout << "Bytes received:" << iResult << "\n";
			std::cout << "Buffer content: " << iResult << " . " << recvbuffer;
		}
		else if (iResult == 0)
			std::cout << "Connection closed";
		else
			std::cout << "recv failed:" << WSAGetLastError();
	} while (iResult > 0);

	// *** cleanup ***
	closesocket(ConnectSocket);
	WSACleanup();

	return isFalse;
}