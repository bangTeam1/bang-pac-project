#ifndef BANG_CARD_H
#define BANG_CARD_H


#include <string>

using namespace std;


// An abstract card entity.
class Card {
private:
	// The original name of the card.
	string original_name_;

	// The translated name of the card.
	string name_;

	// The number of cards in a pack.
	int count_;
public:
	Card();

	Card(string original_name, string name, int count);

	virtual ~Card();

	const string getOriginalName();

	const string getName();

	const int getCount();

	// Returns human-readable name and symbolic description of card
	virtual const string print();
};


#endif //BANG_CARD_H
