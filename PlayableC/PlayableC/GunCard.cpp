#include <vector>
#include "GunCard.h"
#include "Player.h"
#include "Game.h"

#include <iostream>

GunCard::GunCard(string original_name, string name, int count, int distance) : PermanentCard(original_name, name, count), distance_(distance) { }

int GunCard::play(Game *game, Player *player, int position, int target, int target_card) {
	if (player->isPending()) {
		std::cout << "Only the Next tab can now be played.";
	}

	// Remove existing gun if a new GunCard is drawn
	auto & cards = player->getPermanentCards();
	for (unsigned int i = 0; i < cards.size(); i++) {
		if (dynamic_cast<GunCard *>(cards[i].get())) {
			game->discardCard(cards[i]);
			cards.erase(cards.begin() + i);
		}
	}

	player->layCard(position);
	std::cout << "SUCCES!";
	return 0;
}

int GunCard::getDistance() const {
	return distance_;
}

void GunCard::setUnlimitedBang(bool unlimited) {
	unlimited_bang_ = unlimited;
}

bool GunCard::hasUnlimitedBang() const {
	return unlimited_bang_;
}

const string GunCard::print() const {
	string str = print();
	str += " ~Distance~ " + to_string(distance_);
	if (hasUnlimitedBang()) {
		str += "  ~Unlimited Bangs~";
	}
	return str;
}
