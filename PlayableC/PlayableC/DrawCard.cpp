#include <cstdlib>
#include "DrawCard.h"
#include "Game.h"

#include <string>

//Constructor
DrawCard::DrawCard(string original_name, string name, int count, int draw_count)
	: InstantCard(original_name, name, count), draw_count_(draw_count) { }

//Playing Draw Card
int DrawCard::play(Game *game, Player *player, int position, int target, int target_card) {
	if (player->isPending()) {
		std::cout << "INVALID REACTION!";
		return 0;
	}

	if (isTargetable()) {
		// Draw a card from another player
		if ((target_distance_ && game->getDistance(player, target) == 1) || target_any_) {
			Player *target_player = game->getPlayers()[target];

			if (target_player == player || !target_player->isAlive()) {
				std::cout << "INVALID TARGET!";
				return 0;
			}

			if (target_card >= 0) {
				// Draw laid permanent card
				auto & cards = target_player->getPermanentCards();
				if (target_card >= (int)cards.size()) {
					std::cout << "INVALID TARGET CARD!";
					return 0;
				}
				player->addCard(cards[target_card]);
				cards.erase(cards.begin() + target_card);
			}
			else {
				// Draw random card from hand
				if (target_player->getCards().size() == 0) {
					std::cout << "TARGET NO CARDS!";
					return 0;
				}

				target_card = rand() % (int)target_player->getCards().size();
				auto & cards = target_player->getCards();
				player->addCard(cards[target_card]);
				cards.erase(cards.begin() + target_card);
			}
		}
		else {
			std::cout << "OUT OF RAGE!";
			return 0;
		}
	}
	else {
		// Draw a card from pack
		for (int i = 0; i < draw_count_; i++) {
			game->drawCard(player);
		}
	}

	game->discardCard(player, position);

	std::cout << "SUCCES!";
	return 0;
}

// Printing Draw Card
const string DrawCard::print() const {
	string str = print();
	str += "  ";
	for (int i = 0; i < draw_count_; i++) {
		str += "[]";
	}
	if (isTargetable()) {
		if (target_distance_) {
			str += "  ~Distance~";
		}
		else if (target_any_) {
			str += " ~Target-Any~";
		}
	}
	return str;
}

// Sees if card is targetable
bool DrawCard::isCardTargetable() const {
	return isTargetable();
}
