#include "Card.h"

Card::Card()
{
}

Card::Card(string original_name, string name, int count) : original_name_(original_name), name_(name), count_(count) { }

Card::~Card() { }

const string Card::getOriginalName() {
	return original_name_;
}

const string Card::getName() {
	if (name_ == "") {
		return original_name_;
	}
	return name_;
}

const int Card::getCount() {
	return count_;
}

const string Card::print() {
	string str;
	str += getName();
	if (getName() != getOriginalName()) {
		str += " (" + getOriginalName() + ")";
	}
	return str;
}
