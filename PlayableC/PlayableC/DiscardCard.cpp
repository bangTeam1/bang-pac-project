#include <stdlib.h>
#include "DiscardCard.h"
#include "Game.h"

#include <string>

//Constructor
DiscardCard::DiscardCard(string original_name, string name, int count) : InstantCard(original_name, name, count) { }

//Discarding the played card
int DiscardCard::play(Game *game, Player *player, int position, int target, int target_card) {
	if (player->isPending()) {
		std::cout << "Only the Next tab can now be played.";
		return 0;
	}

	Player * target_player = game->getPlayers()[target];

	if (target_player == player || !target_player->isAlive()) {
		std::cout << "Invalid target player code.";
		return 0;
	}

	if (target_card >= 0) {
		auto & cards = target_player->getPermanentCards();
		if (target_card >= (int)cards.size()) {
			std::cout << "Invalid Target Card Code.";
			return 0;
		}
		game->discardCard(cards[target_card]);
		cards.erase(cards.begin() + target_card);
	}
	else {
		if (target_player->getCards().size() > 0) {
			// Discard random card from hand of target player
			target_card = rand() % (int)target_player->getCards().size();
			game->discardCard(target_player, target_card);
		}
		else {
			std::cout << "The target player has no cards in hand.";
			return 0;
		}
	}

	game->discardCard(player, position);

	std::cout << "Succes.";
	return 0;
}

//Make card targetable
bool DiscardCard::isCardTargetable() const {
	return true;
}

//Printing the card
const string DiscardCard::print() const {
	string str = print();
	str += "  [X]";
	return str;
}
