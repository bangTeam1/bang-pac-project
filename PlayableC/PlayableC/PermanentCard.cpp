#include "PermanentCard.h"
#include "Player.h"
#include "Game.h"
#include "Card.h"

#include <iostream>

PermanentCard::PermanentCard(string original_name, string name, int count) : PlayableCard(original_name, name, count) { }

int PermanentCard::play(Game *game, Player *player, int position, int target, int target_card) {
	if (player->isPending()) { // Function declared in player.h as a boolean and it is used to reply a pending action. 
		std::cout << "Only the Next tab can now be played.";
		return 0;
	}

	// if it is not laid, it will be played
	player->layCard(position);
	std::cout << "SUCCESS!";
	return 0; 
}

int PermanentCard::getDistanceTweak() const {
	return distance_tweak_;
}

void PermanentCard::setDistanceTweak(int distance) {
	distance_tweak_ = distance;
}

const string PermanentCard::print() const {
	string str = print();
	if (distance_tweak_) {
		str += "  ";
		if (distance_tweak_ > 0) {
			str += "+";
		}
		str += to_string(distance_tweak_);
	}
	return str;
}