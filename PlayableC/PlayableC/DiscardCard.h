#ifndef BANG_DISCARDCARD_H
#define BANG_DISCARDCARD_H


#include "InstantCard.h"
#include "Card.h"

#include <string>


// An instant card. A player draws a predefined number of cards.
class DiscardCard : public InstantCard {
public:
	DiscardCard(string original_name, string name, int count);
	virtual int play(Game * game, Player * player, int position, int target, int target_card);
	virtual bool isCardTargetable() const;
	virtual const string print() const;
};


#endif //BANG_DISCARDCARD_H