#ifndef BANG_CSVPARSER_H
#define BANG_CSVPARSER_H


#include <vector>
#include <string>

using namespace std;


// A helper class for parsing CSV file into vectors
class CsvParser {
private:
	vector<string> parseRow(string line, char separator);
public:
	vector<vector<string>> parseFile(string filename, char separator, bool ignore_first_line);
};


#endif //BANG_CSVPARSER_H
