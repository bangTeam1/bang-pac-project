#include "BeerCard.h"
#include "Game.h"
#include "Card.h"
#include <iostream>

using namespace std;

BeerCard::BeerCard(string original_name, string name, int count) : InstantCard(original_name, name, count) {}

int BeerCard::play(Game *game, Player *player, int position, int target, int target_card) {

	if (player->isPending() && (player->getLife() > 0 || target_others_)) {
		std::cout<< "Only the Next tab can now be played.";
		return 0;
	}

	//for player_itself_target
	if (target_self_) {
		if (player->getLife() == player->getMaxLife()) {
			std::cout << "You have a full number of lives.";
			return 0;
		}
		player->increaseLife();
	}

	//for all of the players except itself
	if (target_others_) {
		for (auto * item : game->getPlayers()) {
			if (item != player && item->isAlive()) {
				item->increaseLife();
			}
		}
	}


	game->discardCard(player, position);


	if (player->isPending()) {
		player->setPending(false);

		if (game->getPendingPlayers().size() == 0) {
			game->setPendingCard(nullptr);
		}
	}

	return 0; 
}

const string BeerCard::print() const {
	string str = print(); 
	str += " \u2665"; // Unicode Character BLACK HEART SUIT
	return str;
}
