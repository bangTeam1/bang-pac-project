#ifndef BANG_GAMESERVER_H
#define BANG_GAMESERVER_H


#include <map>
#include <thread>
#include "Game.h"
#include "Bot.h"


// A game server which runs on host computer in a separate process.
class GameServer {
private:
	static const string ERROR_ALREADY_RUNNING;
	static const string ERROR_NOT_RUNNING;
	static const string ERROR_SETSOCKOPT;
	static const string ERROR_BIND;
	static const string ERROR_LISTEN;
	static const string ERROR_ACCEPT;
	static const string ERROR_RECV;
	static const string ERROR_SEND;
	static const string ERROR_SUBSCRIBE;
	static const string ERROR_GETADDRINFO;
	static const string ERROR_SOCKET;

	// Game state.
	Game & game_;

	// True if the server is running.
	bool running_ = false;

	// A listenable server socket.
	int socket_;

	// All active socket connections.
	vector<int> connections_;

	// Socket connections subscribed to stream.
	vector<int> stream_connections_;

	// An API socket to player map.
	map<int, Player*> players_;

	// Gets address info for localhost and server port.
	struct addrinfo * getServerInfo();

	// Returns socket descriptor for provided address.
	int getSocket(struct addrinfo * servinfo);

	void waitForConnection();

	void acceptNewConnection();

	bool receiveRequest(int client_socket, string & req);

	string processRequest(string req, int connection);

	void sendResponse(int client_socket, string res);

	void handleUserLeave(vector<int>::iterator it);

	// Sends an event notification to all connected clients.
	void sendEvent(string event);

	void startBot(Bot * bot);

	void handleBot(Bot * bot);
public:
	GameServer(Game & game);

	~GameServer();

	// The port server is listening on.
	static const int PORT = 7768;

	// Min count of players that should be connected before starting a game.
	static const int MIN_PLAYERS = 4;

	// Max count of players that are able to join the game.
	static const int MAX_PLAYERS = 7;

	// Max count of bots.
	static const int MAX_BOTS = 3;

	// The response that is returned by server on successful response-less request.
	static const string SUCCESS;

	// The response is returned by server on error.
	static const string ERROR_;

	// Joining with duplicate name.
	static const string ERROR_JOIN_NAME;

	// Joining when the game already started.
	static const string ERROR_JOIN_PLAYING;

	// Bot limit is reached.
	static const string ERROR_BOT_LIMIT;

	static const string REQ_SUBSCRIBE;
	static const string REQ_JOIN;
	static const string REQ_ADD_BOT;
	static const string REQ_GET_PLAYERS;
	static const string REQ_GET_PLAYERS_INFO;
	static const string REQ_START;
	static const string REQ_GET_CARDS;
	static const string REQ_GET_PERMANENT_CARDS;
	static const string REQ_PLAY_CARD;
	static const string REQ_DISCARD_CARD;
	static const string REQ_FINISH_ROUND;
	static const string REQ_PROCEED;

	static const string EVENT_JOIN;
	static const string EVENT_LEAVE;
	static const string EVENT_START;
	static const string EVENT_NEXT_ROUND;
	static const string EVENT_PROCEED;
	static const string EVENT_PLAY_CARD;
	static const string EVENT_GAME_OVER;

	// Starts the game server.
	void start();

	string playCard(Player * player, int position, int target, int target_card);

	string proceed(Player * player);

	string finishRound();

	// Stops the game server.
	void stop();
};


#endif //BANG_GAMESERVER_H