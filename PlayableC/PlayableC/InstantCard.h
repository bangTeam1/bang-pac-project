#ifndef BANG_INSTANTCARD_H
#define BANG_INSTANTCARD_H


#include "PlayableCard.h"
#include <iostream>
#include <string>


// Carte care se poate juca o singura data si are efect instantaneu
class InstantCard : public PlayableCard {
public:
    InstantCard(std::string original_name, std::string name, int count);
};


#endif //BANG_INSTANTCARD_H
