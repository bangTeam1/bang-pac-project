#include "Application.h"
#include "BootstrapController.h"
#include "GameController.h"
#include "CardParser.h"
#include "signal.h"


Application::~Application() {
	if (server_pid_ != 0) {
		//kill(server_pid_, SIGTERM); //#define SIGTERM         15  -> Software termination signal from kill
		exit((int)server_pid_);
	}
}

void Application::init(string filename) {
	// Load cards pack from file
	vector<Card *> cards = CardsParser().parseFile(filename);
	game_.setPack(cards);

	// Render welcome screen
	BootstrapController(game_, client_, server_pid_).renderWelcome();
	GameController(game_, client_).actionInit();
}
