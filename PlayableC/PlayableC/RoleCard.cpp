#include "RoleCard.h"

RoleCard::RoleCard(string original_name, string name, int count) : Card(original_name, name, count) { }

const string RoleCard::SHERIFF = "Sheriff";
const string RoleCard::DEPUTY = "Deputy";
const string RoleCard::OUTLAW = "Outlow";
const string RoleCard::RENEGATE = "Renegate";
