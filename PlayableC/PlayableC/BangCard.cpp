#include "BangCard.h"
#include "Game.h"

#include <string>

BangCard::BangCard(string original_name, string name, int count) : InstantCard(original_name, name, count) { }

int BangCard::play(Game *game, Player *player, int position, int target, int target_card) {
	if (player->isPending()) {
		std::cout << "Only the Next tab can now be played.";
		return 0;
	}

	if (target_distance_) {
		Player *target_player = game->getPlayers()[target];
		if (target_player == player || !target_player->isAlive()) {
			std::cout << "Invalid target player code.";
			return 0;
		}

		// the played card is a Bang!
		if (player->hasPlayedBang() && !player->hasUnlimitedBang()) {
			std::cout << "This round is a Bang! played.";
			return 0;
		}

		int distance = game->getDistance(player, target);
		int range = player->getGunRange();

		// if the distance is bigger than the range of the weapon, the player cannot be targetable
		if (distance > range) {
			std::cout << "The target player is too far away.";
			return 0;
		}

		// put Bang! card in a pending state to see if everything is ok and the card can be played
		game->setPendingCard(this);
		target_player->setPending(true);
		game->discardCard(player, position);
		player->setPlayedBang(true);
	}
	else if (target_others_) {
		// verifies if the Bang! card has an effect on the other players
		game->setPendingCard(this);
		for (auto *p : game->getPlayers()) {
			if (p != player && p->isAlive()) {
				p->setPending(true);
			}
		}//Bang! card is discarded after the usage 
		game->discardCard(player, position);
	}
	else {
		std::cout << "Invalid card definition.";
		return 0;
	}

	std::cout << "SUCCES!";
	return 0;
}

bool BangCard::proceed(Game *game, Player *player) {
	player->decreaseLife();
	return true; // the effect of the Bang! card
}