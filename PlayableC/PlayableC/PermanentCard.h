#ifndef BANG_PERMANENTCARD_H
#define BANG_PERMANENTCARD_H


#include "PlayableCard.h"
#include "Card.h"

// A card which has a permanent affect while he is placed on the board
// When removed, the affect is no longer available.

class PermanentCard : public PlayableCard {
private:
	int distance_tweak_ = 0;
public:
	PermanentCard(string original_name, string name, int count);
	virtual int play(Game *game, Player *player, int position, int target, int target_card);
	int getDistanceTweak() const;
	void setDistanceTweak(int distance);
	virtual const string print() const;
};


#endif //BANG_PERMANENTCARD_H