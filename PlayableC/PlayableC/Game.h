#ifndef BANG_GAME_H
#define BANG_GAME_H


#include "Player.h"
#include "Card.h"
#include "CharacterCard.h"
#include "RoleCard.h"
#include "PlayableCard.h"

#include <list>
#include <vector>
#include <chrono>


class Game {
private:
	vector<shared_ptr<RoleCard>> roles_;
	vector<shared_ptr<CharacterCard>> characters_;
	vector<shared_ptr<PlayableCard>> cards_;
	list<shared_ptr<PlayableCard>> pack_;
	vector<Player *> players_;
	Player * player_on_turn_ = nullptr;
	Player * me_;

	PlayableCard * pending_card_ = nullptr;

	// Assigns roles to players. (Sheriff, Outlaw, Renegate, Deputy) it depends of the number of the players.
	void assignRoles();

	// Assigns characters to players. ( there are 16 character and for each player it is assigned only one, with a special abiltiy)
	void assignCharacters();

	// Assigns cards to players (it depends of the number of his life Points)
	void assignCards();

	// Sets players default life points to max value, max value is the number of bullets assigned to every character card.
	void setLifePoints();

	// Gets player's position.
	const int getPlayerPosition(Player * player) const;

public:


	static const int SUCCESS = 0;
	static const int ERROR_OUT_OF_RANGE = 1;
	static const int ERROR_INVALID_CARD = 2;
	static const int ERROR_INVALID_TARGET = 3;
	static const int ERROR_INVALID_TARGET_CARD = 4;
	static const int ERROR_BANG_LIMIT = 5;
	static const int ERROR_TARGET_NO_CARDS = 6;
	static const int ERROR_CARD_ALREADY_LAID = 7;
	static const int ERROR_INVALID_MISS = 8;
	static const int ERROR_INVALID_REACTION = 9;
	static const int ERROR_BEER_NO_EFFECT = 10;
	static const int ERROR_BEER_AVAILABLE = 11;
	static const int ERROR_UNKNOWN_CARD = 12;
	static const int ERROR_UNKNOWN = 100;


	Game();

	~Game();

	// Sets current player and we add the new players in a vector.
	void setMe(Player * player);

	// used to return the current player
	Player * getMe();

	// We add a new player in a vector.
	void addPlayer(Player * player);

	// The array of the players.
	vector<Player *> & getPlayers();

	// Gets pending players.
	vector<Player *> getPendingPlayers();

	/// Gets opponents for target player.
	vector<Player *> getOpponents(Player * player);

	/// Gets number of bots.
	int getBotsCount() const;

	// Sets cards pack.
	void setPack(vector<Card *> cards);

	// Sets a pending card.
	void setPendingCard(PlayableCard * card);

	// Returns a pending card.
	PlayableCard * getPendingCard();

	// Initializes the roles, characters, lifepoints and cards.
	void initGame();

	// Finds cards in pack by their original names.
	vector<shared_ptr<PlayableCard>> getCardsByNames(vector<string> names) const;

	// Finds a player by name.
	Player * getPlayer(string name);

	// Finds a card by name.
	shared_ptr<PlayableCard> getCard(string name);

	// Gets player on turn.
	Player * getPlayerOnTurn();

	// Returns players with specific role.
	vector<Player *> getPlayersByRole(string role);

	// Gets winning roles if game is over.
	vector<string> getWinners();

	// Computes a distance between two players.
	const int getDistance(Player *player, int target) const;

	// Updates player info. Should be called by client when received updated info from server.
	bool updatePlayer(string name, unsigned int life, string character, string role, bool on_turn, bool pending, unsigned int cards);

	// Updates player's permanent cards. Should be called by client when received updated info from server.
	bool updatePermanentCards(string name, vector<string> cards);

	// A player draws a card.
	void drawCard(Player * player);

	/**
	 * \brief Discards a card from hand.
	 * \param player A player that wants to discard a card.
	 * \param position A position of the card in hand.
	 */
	bool discardCard(Player * player, int position);

	// Places a card to the bottom of pack.
	void discardCard(shared_ptr<PlayableCard> card);

	// Plays a card.
	int playCard(Player * player, int position, int target = -1, int target_card = -1);

	// Finishes current round and hands control over to the next player.
	void finishRound();

	// Does not reply to pending card.
	void proceed(Player * player);

	/// Converts an error code into human-readable error message.
	static string getErrorMessage(int code);

	// Computes current game state utility for player.
	int computeUtility(Player * player) const;

	// Creates a deep copy of self.
	Game clone() const;
};

#endif //BANG_GAME_H


	
